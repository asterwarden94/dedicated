import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppDashboardComponent } from './components/app-dashboard/app-dashboard.component';
import { SolutionsComponent } from './components/solutions/solutions.component';
import { ProjectDescriptionComponent } from './components/project-description/project-description.component';
import { FalseLinkButtonComponent } from './components/false-link-button/false-link-button.component';

@NgModule({
  declarations: [
    AppComponent,
    AppDashboardComponent,
    SolutionsComponent,
    ProjectDescriptionComponent,
    FalseLinkButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
