import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.scss']
})
export class SolutionsComponent implements OnInit {

  mock = [
    {
      imageLink: '../../../assets/images/Software.svg',
      name: 'Software Development',
      description: 'Our bespoke solutions are designed to identify and solve problem areas for your customers, while showcasing what makes your business unique.'
    },
    {
      imageLink: '../../../assets/images/MobileApps.svg',
      name: 'Mobile Development',
      description: 'Alleviate your market’s pain points with a mobile app designed ahead of the latest software trends.'
    },
    {
      imageLink: '../../../assets/images/Products.svg',
      name: 'MVP Development',
      description: 'Bring your idea to life and meet the needs of early adopters with a functioning Minimum Viable Product. We deliver a fast product launch with fewer expenses to your pocket.'
    },
    {
      imageLink: '../../../assets/images/CustomWebsites.svg',
      name: 'Web Development',
      description: 'Boost your digital impact with a secure, scalable and user-driven website. Our web development specialists will tailor your new website specifically for your target audience.'
    },
    {
      imageLink: '../../../assets/images/Support.svg',
      name: 'Technical Support',
      description: 'Ensure the best user experience and enhance your project performance with our 24/7 client-oriented specialists and advisors'
    },
    {
      imageLink: '../../../assets/images/Techteam.svg',
      name: 'Dedicated Tech Teams',
      description: 'Leave the maintenance and production to our team of technical experts, we’ll let you sit back to enjoy the solutions and effective results.\n'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
