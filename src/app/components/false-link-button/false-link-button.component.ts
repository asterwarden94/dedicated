import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-false-link-button',
  templateUrl: './false-link-button.component.html',
  styleUrls: ['./false-link-button.component.scss']
})
export class FalseLinkButtonComponent implements OnInit {

  @Input() name = 'Get started now';

  constructor() { }

  ngOnInit(): void {
  }

}
