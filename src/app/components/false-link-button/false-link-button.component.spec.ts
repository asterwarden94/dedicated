import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FalseLinkButtonComponent } from './false-link-button.component';

describe('FalseLinkButtonComponent', () => {
  let component: FalseLinkButtonComponent;
  let fixture: ComponentFixture<FalseLinkButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FalseLinkButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FalseLinkButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
