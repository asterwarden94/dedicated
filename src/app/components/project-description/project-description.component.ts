import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-description',
  templateUrl: './project-description.component.html',
  styleUrls: ['./project-description.component.scss']
})
export class ProjectDescriptionComponent implements OnInit {

  mock = [
    {
      imageLink: '../../../assets/images/Illustration.svg',
      stepName: 'Project plan drafting',
      desciptionList: [
        {
          value: 'сreate a list of the main product sections'
        },
        {
          value: 'determine the functions of each section (it’s better to form functions as user stories)'
        },
        {
          value: 'set the date of the final product release'
        },
        {
          value: 'make a plan of sprints including the priority features'
        },
        {
          value: 'choose a contractor'
        },
        {
          value: 'define the technology stack'
        },
        {
          value: 'make a resource plan, specifying which resources do you need and when.'
        },
      ]
    },
    {
      imageLink: '../../../assets/images/Illustration.svg',
      stepName: 'Project plan drafting',
      desciptionList: [
        {
          value: 'сreate a list of the main product sections'
        },
        {
          value: 'determine the functions of each section (it’s better to form functions as user stories)'
        },
        {
          value: 'set the date of the final product release'
        },
        {
          value: 'make a plan of sprints including the priority features'
        },
        {
          value: 'choose a contractor'
        },
        {
          value: 'define the technology stack'
        },
        {
          value: 'make a resource plan, specifying which resources do you need and when.'
        },
      ]
    },
    {
      imageLink: '../../../assets/images/Illustration.svg',
      stepName: 'Project plan drafting',
      desciptionList: [
        {
          value: 'сreate a list of the main product sections'
        },
        {
          value: 'determine the functions of each section (it’s better to form functions as user stories)'
        },
        {
          value: 'set the date of the final product release'
        },
        {
          value: 'make a plan of sprints including the priority features'
        },
        {
          value: 'choose a contractor'
        },
        {
          value: 'define the technology stack'
        },
        {
          value: 'make a resource plan, specifying which resources do you need and when.'
        },
      ]
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
